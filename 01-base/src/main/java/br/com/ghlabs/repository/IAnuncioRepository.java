package br.com.ghlabs.repository;

import java.util.List;

import br.com.ghlabs.models.*;

public interface IAnuncioRepository {

	public void inserir(Anuncio anuncio);
	public boolean remover(Integer codAnuncio);
	public boolean alterar(Integer codAnuncio);
	public List<Anuncio> obterTodos();	
	
}
