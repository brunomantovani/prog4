package br.com.ghlabs.repository;

import java.util.List;

import br.com.ghlabs.models.Usuario;

public interface IUsuarioRepository {

	public void inserir(Usuario usuario);
	public boolean remover(Integer codUsuario);
	public boolean alterar(Integer codUsuario);
	public List<Usuario> obterTodos();	
	
}
