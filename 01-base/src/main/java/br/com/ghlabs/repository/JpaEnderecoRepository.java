package br.com.ghlabs.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import br.com.ghlabs.models.Anuncio;
import br.com.ghlabs.models.Endereco;

//@Primary
@Repository
public class JpaEnderecoRepository  implements IEnderecoRepository{

	@PersistenceContext(name="apetPU")
	private EntityManager entityManager;
	
	
	@Override
	public void inserir(Endereco endereco) {
		entityManager.merge(endereco);
		
	}

	@Override
	public List<Anuncio> obterTodos() {
		String jql = "SELECT a FROM Endereco e";
		Query q;
		q = entityManager.createQuery(jql, Endereco.class);
		return q.getResultList();
	}

}
