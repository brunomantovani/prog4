package br.com.ghlabs.repository;

import java.util.List;

import br.com.ghlabs.models.Anuncio;
import br.com.ghlabs.models.Endereco;

public interface IEnderecoRepository {

	public void inserir(Endereco endereco);
	public List<Anuncio> obterTodos();	
	
}
