package br.com.ghlabs.controllers;

import br.com.ghlabs.models.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ghlabs.repository.IAnuncioRepository;
import br.com.ghlabs.repository.JpaAnuncioRepository;

@Controller
public class AnunciosController {
	
	private List<Anuncio> anuncio;
	
	@Autowired
	private IAnuncioRepository repo;
	
	@RequestMapping("/anuncios")
	public String anuncios(Model model)
	{
		anuncio = repo.obterTodos();
		
		System.out.println(anuncio.toString());
		
		model.addAttribute("listaAnuncio", anuncio);
		
		return "anuncios";
	}
	

}
