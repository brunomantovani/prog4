package br.com.ghlabs.models;
// Generated 15/09/2016 16:48:28 by Hibernate Tools 5.2.0.Beta1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "endereco", catalog = "apetjpa")
public class Endereco{

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "codEndereco", unique = true, nullable = false)
	private Integer codEndereco;
	
	@Column(name = "Rua", length = 45)
	@Size(min=5, message="Digite um nome valido")
	private String rua;
	
	@Column(name = "Bairro", length = 45)
	@Size(min=5, message="Digite um nome valido")
	private String bairro;
	
	@Column(name = "Numero", length = 45)
	private String numero;

	@Column(name = "CEP", length = 45)
	private String cep;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Usuario_codUsuario", nullable = false)
	private Usuario usuario;
	
	public Endereco() {
	}

	public Endereco(Usuario usuario) {
		this.usuario = usuario;
	}

	public Endereco(Usuario usuario, String rua, String bairro, String numero, String cep) {
		this.usuario = usuario;
		this.rua = rua;
		this.bairro = bairro;
		this.numero = numero;
		this.cep = cep;
	}
	
		public Integer getCodEndereco() {
		return this.codEndereco;
	}

	public void setCodEndereco(Integer codEndereco) {
		this.codEndereco = codEndereco;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	
	public String getRua() {
		return this.rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	
	public String getBairro() {
		return this.bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}


	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	
	public String getCep() {
		return this.cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	@Override
	public String toString() {
		return "Endereco [codEndereco=" + codEndereco + ", rua=" + rua + ", bairro=" + bairro + ", numero=" + numero
				+ ", cep=" + cep + ", usuario=" + usuario + "]";
	}
	
	
}
