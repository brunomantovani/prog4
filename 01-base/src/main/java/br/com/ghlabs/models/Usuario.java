package br.com.ghlabs.models;
// Generated 15/09/2016 16:48:28 by Hibernate Tools 5.2.0.Beta1

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

import com.sun.istack.internal.NotNull;

@Entity
@Table(name = "usuario", catalog = "apetjpa", uniqueConstraints = @UniqueConstraint(columnNames = "Email"))
public class Usuario {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "codUsuario", unique = true, nullable = false)
	private Integer codUsuario;

	@Column(name = "Nome", length = 45)
	@Size(min = 5, message = "Digite um nome valido")
	private String nome;

	@Column(name = "Email", unique = true, length = 45)
	@Size(min = 5, message = "Digite um email valido")
	private String email;

	@Column(name = "Senha", length = 45)
	private String senha;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "usuario")
	private List<Anuncio> anuncios;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "usuario")
	private List<Endereco> enderecos;

	public Usuario() {
	}

	public Usuario(Integer codUsuario, String nome, String email, String senha, List<Anuncio> anuncios,
			List<Endereco> enderecos) {
		super();
		this.codUsuario = codUsuario;
		this.nome = nome;
		this.email = email;
		this.senha = senha;
		this.anuncios = anuncios;
		this.enderecos = enderecos;
	}

	public Integer getCodUsuario() {
		return this.codUsuario;
	}

	public void setCodUsuario(Integer codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return this.senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public List<Anuncio> getAnuncios() {
		return anuncios;
	}

	public void setAnuncios(List<Anuncio> anuncios) {
		this.anuncios = anuncios;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	@Override
	public String toString() {
		return "Usuario [codUsuario=" + codUsuario + ", nome=" + nome + ", email=" + email + ", senha=" + senha
				+ ", anuncios=" + anuncios + ", enderecos=" + enderecos + "]";
	}
	
	
}
